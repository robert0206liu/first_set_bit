#include <stdio.h>

int FindFirstSetBit(int value)
{
    static int index = 0;
    
    if(value == 0)
    {
        printf("The value is ZERO\n");
        return -1;
    }
    
    if((value & 1) == 0)
    {
        index++;
        return FindFirstSetBit(value>>1);
    }
    return index;
}

int main()
{
    int value = 0x00000100;
    
    printf("First set bit at %d\n", FindFirstSetBit(value));
    
    return 0;
}